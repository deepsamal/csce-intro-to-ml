import numpy as np
import math


class MSELoss:      # For Reference
    def __init__(self):
        # Buffers to store intermediate results.
        self.current_prediction = None
        self.current_gt = None
        pass

    def __call__(self, y_gt, y_pred):
        self.current_prediction = y_pred
        self.current_gt = y_gt

        # MSE = 0.5 x (GT - Prediction)^2
        loss = 0.5 * np.power((y_gt - y_pred), 2)
        return loss

    def grad(self, y_gt, y_pred):
        # Derived by calculating dL/dy_pred
        gradient = -1 * (self.current_gt - self.current_prediction)

        # We are creating and emptying buffers to emulate computation graphs in
        # Modern ML frameworks such as Tensorflow and Pytorch. It is not required.
        self.current_prediction = None
        self.current_gt = None

        return gradient


class CrossEntropyLoss:     # TODO: Make this work!!!
    def __init__(self):
        # Buffers to store intermediate results.
        self.current_prediction = None
        self.current_gt = None
        pass

    def __call__(self, y_gt, y_pred):
        # TODO: Calculate Loss Function
        y_pred = np.clip(y_pred, 1e-15, 1 - 1e-15)
        loss = - y_gt * np.log(y_pred) - (1 - y_gt) * np.log(1 - y_pred)
        # y_gt[y_gt == 0] = 0.01
        # loss = -1 * np.sum(y_gt * np.log(y_pred))
        return loss

    def grad(self, y_gt, y_pred):
        # TODO: Calculate Gradients for back propagation
        y_pred = np.clip(y_pred, 1e-15, 1 - 1e-15)
        gradient = - (y_gt / y_pred) + (1 - y_gt) / (1 - y_pred)
        # y_gt[y_gt == 0] = 0.01
        # gradient = - y_gt * (1 / y_pred)
        return gradient


class SoftmaxActivation:    # TODO: Make this work!!!
    def __init__(self):
        pass

    def __call__(self, y):
        # TODO: Calculate Activation Function
        e_y = np.exp(y - np.max(y, axis=-1, keepdims=True))
        out = e_y / np.sum(e_y, axis=-1, keepdims=True)
        return out

    def grad(self, x):
        # TODO: Calculate Gradients.. Remember this is calculated w.r.t. input to the function -> dy/dz
        p = self.__call__(x)
        return p * (1 - p)


class SigmoidActivation:    # TODO: Make this work!!!
    def __init__(self):
        pass

    def __call__(self, y):
        # TODO: Calculate Activation Function
        out = 1 / (1 + np.exp(-y))
        return out

    def grad(self, x):
        # TODO: Calculate Gradients.. Remember this is calculated w.r.t. input to the function -> dy/dz
        return self.__call__(x) * (1 - self.__call__(x))


class ReLUActivation:
    def __init__(self):
        self.z = None
        pass

    def __call__(self, z):
        # y = f(z) = max(z, 0) -> Refer to the computational model of an Artificial Neuron
        self.z = z
        y = np.maximum(z, 0)
        return y

    def __grad__(self):
        # dy/dz = 1 if z was > 0 or dy/dz = 0 if z was <= 0
        gradient = np.where(self.z > 0, 1, 0)
        return gradient


def accuracy_score(y_true, y_pred):
    """ Compare y_true to y_pred and return the accuracy """
    accuracy = np.sum(y_true == y_pred, axis=0) / len(y_true)
    return accuracy
