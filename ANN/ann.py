import os, sys
import numpy as np
import math
import matplotlib.pyplot as plt

from data import readDataLabels, normalize_data, train_test_split, to_categorical
from utils import accuracy_score
from utils import SigmoidActivation, SoftmaxActivation, ReLUActivation, CrossEntropyLoss, MSELoss

# Create an MLP with 8 neurons
# Input -> Hidden Layer -> Output Layer -> Output
# Neuron = f(w.x + b)
# Do forward and backward propagation

mode = 'train'      # train/test... Optional mode to avoid training incase you want to load saved model and test only.

class ANN:
    def __init__(self, num_input_features, num_hidden_units, num_outputs, hidden_unit_activation, output_activation, loss_function):
        self.num_input_features = num_input_features
        self.num_hidden_units = num_hidden_units
        self.num_outputs = num_outputs

        self.hidden_unit_activation = hidden_unit_activation
        self.output_activation = output_activation
        self.loss_function = loss_function


    def initialize_weights(self):   # TODO
        # Create and Initialize the weight matrices
        # Never initialize to all zeros. Not Cool!!!
        # Try something like uniform distribution. Do minimal research and use a cool initialization scheme.

        # Hidden layer
        limit = 1 / math.sqrt(self.num_input_features)
        self.w1  = np.random.uniform(-limit, limit, (self.num_input_features, self.num_hidden_units))
        self.b1 = np.zeros((1, self.num_hidden_units))

        # Output layer
        limit = 1 / math.sqrt(self.num_hidden_units)
        self.w2  = np.random.uniform(-limit, limit, (self.num_hidden_units, self.num_outputs))
        self.b2 = np.zeros((1, self.num_outputs))

        return

    def forward(self, X):      # TODO
        # x = input matrix
        # hidden activation y = f(z), where z = w.x + b
        # output = g(z'), where z' =  w'.y + b'
        # Trick here is not to think in terms of one neuron at a time
        # Rather think in terms of matrices where each 'element' represents a neuron
        # and a layer operation is carried out as a matrix operation corresponding to all neurons of the layer

        # Input/Hidden Layer
        self.X = X
        self.hidden_input = X.dot(self.w1) + self.b1
        self.hidden_output = self.hidden_unit_activation(self.hidden_input)

        # Output Layer
        self.output_layer_input = self.hidden_output.dot(self.w2) + self.b2
        self.y_pred = self.output_activation(self.output_layer_input)

        return self.y_pred

    def backward(self, y_gt, y_pred):     # TODO
        # Grad. w.r.t input of output layer
        grad_wrt_out_l_input = self.loss_function.grad(y_gt, y_pred) * self.output_activation.grad(self.output_layer_input)
        self.grad_w2 = self.hidden_output.T.dot(grad_wrt_out_l_input)
        self.grad_b2 = np.sum(grad_wrt_out_l_input, axis=0, keepdims=True)

        # HIDDEN LAYER
        # Grad. w.r.t input of hidden layer
        grad_wrt_hidden_l_input = grad_wrt_out_l_input.dot(self.w2.T) * self.hidden_unit_activation.grad(self.hidden_input)
        self.grad_w1 = self.X.T.dot(grad_wrt_hidden_l_input)
        self.grad_b1 = np.sum(grad_wrt_hidden_l_input, axis=0, keepdims=True)

    def update_params(self, learning_rate):    # TODO
        # Take the optimization step.
        self.w1 -= learning_rate * self.grad_w1
        self.b1 -= learning_rate * self.grad_b1
        self.w2 -= learning_rate * self.grad_w2
        self.b2 -= learning_rate * self.grad_b2
        return

    def train(self, dataset, learning_rate=0.001, num_epochs=1000):
        X, y = dataset
        self.initialize_weights()
        losses = []
        accuracies = []

        for epoch in range(num_epochs):
            # Forward
            y_pred = self.forward(X)

            loss = self.loss_function(y, y_pred)
            losses.append(loss)

            # Backward
            self.backward(y, y_pred)

            # Update Parameters
            self.update_params(learning_rate)

            accuracies.append(self.test(dataset))

        # plt.plot(losses)
        # print(accuracies[-1])
        # plt.plot(accuracies)
        # plt.show()
        return losses


    def test(self, dataset):
        accuracy = 0    # Test accuracy
        # Get predictions from test dataset
        # Calculate the prediction accuracy, see utils.py
        X, y = dataset
        y_pred = np.argmax(self.forward(X), axis=1)
        y_gt = np.argmax(y, axis=1)
        accuracy = np.sum(y_gt == y_pred, axis=0) / len(y_gt)
        return accuracy


def main(argv):
    # Load dataset
    dataset = readDataLabels()      # dataset[0] = X, dataset[1] = y

    X = dataset[0]
    y = dataset[1]

    # Normalize data
    X = normalize_data(X)

    # Convert the nominal y values to binary
    y = to_categorical(y)

    # Split data into train and test split. call function in data.py
    X_train, X_test, y_train, y_test = train_test_split(X, y, n=0.4)

    # Define ANN
    hidden_activation = SigmoidActivation()
    output_activation = SoftmaxActivation()
    loss_function = CrossEntropyLoss()
    ann = ANN(64, 16, 10, hidden_activation, output_activation, loss_function)

    # call ann->train()... Once trained, try to store the model to avoid re-training everytime
    if mode == 'train':
        ann.train(dataset=(X_train, y_train))
        print('Test Accuracy: ', ann.test(dataset=(X_test, y_test)))
        pass        # Call ann training code here
    else:
        # Call loading of trained model here, if using this mode (Not required, provided for convenience)
        raise NotImplementedError

    # Call ann->test().. to get accuracy in test set and print it.


if __name__ == "__main__":
    main(sys.argv)
