import os, sys
import numpy as np
from sklearn import datasets

#import libraries as needed

def readDataLabels(): 
	#read in the data and the labels to feed into the ANN
	data = datasets.load_digits()
	X = data.data
	y = data.target

	return X,y

def to_categorical(y):
	
	#Convert the nominal y values tocategorical
	""" One-hot encoding of nominal values """
	n_col = np.amax(y) + 1
	one_hot = np.zeros((y.shape[0], n_col))
	one_hot[np.arange(y.shape[0]), y] = 1
	return one_hot
	
def train_test_split(data,labels,n=0.8): #TODO

	#split data in training and testing sets
	split_i = len(labels) - int(len(labels) // (1 / n))
	X_train, X_test = data[:split_i], data[split_i:]
	y_train, y_test = labels[:split_i], labels[split_i:]

	return X_train, X_test, y_train, y_test

def normalize_data(data): #TODO

	# normalize/standardize the data
	""" Normalize the dataset X """
	l2 = np.atleast_1d(np.linalg.norm(data, ord=2, axis=1))
	l2[l2 == 0] = 1
	return data / np.expand_dims(l2, axis=1)

